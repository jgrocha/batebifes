const SerialPort = require('serialport')
const port = new SerialPort('/dev/ttyS10', { })
port.on('open', function () { console.log('Port open'); });

msg = process.argv[2];
payload = process.argv[3];

var signals = {
    'START':    'bicafe/din/1',
    'STOP':     'bicafe/din/2', 
    'AUT':      'bicafe/din/3', 
    'SWTOP':    'bicafe/din/4', 
    'SWLOW':    'bicafe/din/5', 
    'SWLEFT':   'bicafe/din/6', 
    'SWRIGHT':  'bicafe/din/7', 
    'MOTPROT':  'bicafe/din/8', 
    'SWFLEFT':  'bicafe/din/9', 
    'SWFRIGHT': 'bicafe/din/10', 
    'EMERG':    'bicafe/din/11', 
    'SWALEFT':  'bicafe/din/12', 
    'SWARIGHT': 'bicafe/din/13'
};

if (msg in signals) {
    pic32msg = signals[msg];
    port.write(`${msg} ${payload}`);
} else {
    console.log(`Unrecignized message ${msg}`)
}

/*
: node sendfakeserial.js bicafe/din/1 on 
START: node sendfakeserial.js START on 
*/

/*
-- sudo socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11 

sudo vi /etc/systemd/system/socat-virtual-ports.service

[Unit]
Description=Creates two virtual serial ports ttyS10 and /dev/ttyS11

[Service]
ExecStart=/usr/bin/socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11

[Install]
WantedBy=multi-user.target

sudo systemctl daemon-reload
sudo systemctl enable socat-virtual-ports
sudo systemctl start socat-virtual-ports

/dev/ttyS10 -> /dev/pts/4
/dev/ttyS11 -> /dev/pts/5

sudo chmod a+rw /dev/pts/4
sudo chmod a+rw /dev/pts/5
*/