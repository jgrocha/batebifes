const SerialPort = require('serialport')
const port = new SerialPort('/dev/ttyS11', {})

const parsers = SerialPort.parsers
const parser = new parsers.Readline({ delimiter: '\n' })
port.pipe(parser)

port.on('open', function () { console.log('Port open'); });
port.on('data', (data) => {
  console.log('Received:\t', data.toString());
});