const SerialPort = require('serialport')
const port = new SerialPort('/dev/ttyS10', {})

const parsers = SerialPort.parsers
const parser = new parsers.Readline({ delimiter: '\n' })
port.pipe(parser)

port.on('open', function () { console.log('Port open'); });

msg = process.argv[2];
payload = process.argv[3];

var signals = {
    'START': 'bicafe/din/1',
    'STOP': 'bicafe/din/2',
    'AUT': 'bicafe/din/3',
    'SWTOP': 'bicafe/din/4',
    'SWLOW': 'bicafe/din/5',
    'SWLEFT': 'bicafe/din/6',
    'SWRIGHT': 'bicafe/din/7',
    'MOTPROT': 'bicafe/din/8',
    'SWFLEFT': 'bicafe/din/9',
    'SWFRIGHT': 'bicafe/din/10',
    'EMERG': 'bicafe/din/11',
    'SWALEFT': 'bicafe/din/12',
    'SWARIGHT': 'bicafe/din/13'
};

send = function (msg, payload) {
    if (msg in signals) {
        pic32msg = signals[msg];
        port.write(`${pic32msg} ${payload}`);
    } else {
        console.log(`Unrecognized message ${msg}`)
    }
}

var outputsidx = ['LON', 'LOFF', 'LALARM', 'MOTOR', 'VLOW', 'VTOP', 'VLEFT', 'VRIGHT'];

var queue = [];

port.on('data', (data) => {
    body = data.toString().split(' ')
    msg = body[0];
    payload = body[1];
    path = msg.split('/');
    index = path[2];
    // console.log(index, outputsidx.length, path[0], path[1], path[2])
    if (path[1] == 'dout') {
        if (index >= 1 && index <= outputsidx.length) {
            console.log('Signal ' + outputsidx[index - 1] + ' ' + payload + ' received on digital port ' + index);
        }
    }
    // asket to read some input
    if (path[1] == 'din') {
        // fake! we aswer that it is on
        queue.push(msg + ' on');
        // port.write(msg + ' on');
    }
    console.log('Received:\t', data.toString());
});

setInterval(function () {
    if (queue.length > 0) {
        port.write(queue.shift());
    }
}, 500);

/*
node -i -e "$(< send.js)"
*/