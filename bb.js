const EventEmitter = require('events');
class MyEmitter extends EventEmitter { }
const pic32 = new MyEmitter();

// sudo chmod a+rw /dev/ttyS10
// sudo chmod a+rw /dev/ttyS11
const SerialPort = require('serialport')
const port = new SerialPort('/dev/ttyS11', {})

const parsers = SerialPort.parsers
const parser = new parsers.Readline({ delimiter: '\n' })
port.pipe(parser)

var START, STOP, AUT, SWTOP, SWLOW, SWLEFT, SWRIGHT, MOTPROT, SWFLEFT, SWFRIGHT, EMERG, SWALEFT, SWARIGHT;
var inputs = [START, STOP, AUT, SWTOP, SWLOW, SWLEFT, SWRIGHT, MOTPROT, SWFLEFT, SWFRIGHT, EMERG, SWALEFT, SWARIGHT];
var signals = ['START', 'STOP', 'AUT', 'SWTOP', 'SWLOW', 'SWLEFT', 'SWRIGHT', 'MOTPROT', 'SWFLEFT', 'SWFRIGHT', 'EMERG', 'SWALEFT', 'SWARIGHT'];
for (var i = 0; i < inputs.length; i++) {
  inputs[i] = 'off';
}

var outputs = {
  'LON': 'cafeina/dout/1',
  'LOFF': 'cafeina/dout/2',
  'LALARM': 'cafeina/dout/3',
  'MOTOR': 'cafeina/dout/4',
  'VLOW': 'cafeina/dout/5',
  'VTOP': 'cafeina/dout/6',
  'VLEFT': 'cafeina/dout/7',
  'VRIGHT': 'cafeina/dout/8'
};

port.on('open', function () { console.log('Port open'); });

port.on('data', (data) => {
  body = data.toString().split(' ')
  msg = body[0];
  payload = body[1];
  path = msg.split('/');
  index = path[2];
  if (path[1] == 'din') {
    if (index >= 1 && index <= inputs.length) {
      inputs[index - 1] = payload;
      console.log('Signal ' + signals[index - 1] + ' ' + payload + ' received on digital port ' + index);
      pic32.emit(signals[index - 1], signals[index - 1], index, payload);
    }
  }
  console.log('Received:\t', data.toString());
});

for (var i = 0; i < signals.length; i++) {
  pic32.on(signals[i], function (signal, index, payload) {
    console.log(signal + ' --(' + (index) + ')--> ' + payload);
  });
}

// Alternativa A
// https://italonascimento.github.io/applying-a-timeout-to-your-promises/
const timeoutPromise = function (ms, promise) {

  // Create a promise that rejects in <ms> milliseconds
  let timeout = new Promise((resolve, reject) => {
    let id = setTimeout(() => {
      clearTimeout(id);
      reject('Timed out in ' + ms + 'ms.')
    }, ms)
  })

  // Returns a race between our timeout and the passed in promise
  return Promise.race([
    promise,
    timeout
  ])
}

// Alternativa B
// https://gist.github.com/john-doherty/bcf35d39d8b30d01ae51ccdecf6c94f5
function promiseTimeout(ms, promise) {
  return new Promise((resolve, reject) => {
    // setTimeout(() => reject(new Error("promise timeout")), ms);
    setTimeout(() => reject(), ms);
    promise.then(resolve).catch(reject);
  });
}

var StateMachine = require('@taoqf/javascript-state-machine');
var visualize = require('./javascript-state-machine/src/plugin/visualize');

// https://github.com/jakesgordon/javascript-state-machine/blob/master/docs/async-transitions.md
// Estudar Promise
// https://italonascimento.github.io/applying-a-timeout-to-your-promises/

var bb = new StateMachine({
  init: 'arranque',
  transitions: [
    { name: 'prepara', from: 'arranque', to: 'iniciociclo' },
    { name: 'bate', from: 'iniciociclo', to: 'umquarto' },
    { name: 'sobeeempurra', from: 'umquarto', to: 'meiociclo' },
    { name: 'bate', from: 'meiociclo', to: 'tresquartos' },
    { name: 'sobeepuxa', from: 'tresquartos', to: 'iniciociclo' },
    // de qualquer estado (incluindo emergencia), pode-se transitar para o estado emergencia
    { name: 'erro', from: '*', to: 'emergencia' },
    { name: 'recupera', from: 'emergencia', to: 'arranque' }
  ],
  methods: {
    // onPrepara:      function() { console.log('A garantir que o tabuleiro está à esquerda e a prensa em cima')    },
    onBate: function () { console.log('Prensa em baixo') },
    onSobeeempurra: function () { console.log('Prensa em cima e tabuleiro à direita') },
    onSobeepuxa: function () { console.log('Prensa em cima e tabuleiro à esquerda') },
    onErro: function () { console.log('Emergência') },
    onRecupera: function () { console.log('No estado inicial de arranque') },

    /* onBeforePrepara: function (lifecycle) {
      return new Promise(function(resolve, reject) {
        if (SWALEFT && SWARIGHT)
          resolve()
        else
          reject()
      })
    }, */

    onBeforePrepara: function (lifecycle) {
      return new Promise((resolve, reject) => {
        setTimeout(() => reject('uh oh'), 2000);
      }).catch(() => { });
    },

    /* onBeforePrepara: function (lifecycle) {
      if (SWALEFT && SWARIGHT)
        return true
      else
        return false
    }, */

    onAfterPrepara: function (lifecycle) {
      console.log('Acende luz verde')
    },

    // Só salta para o estado 'umquarto' se receber o evento 'DOWN'
    onLeaveIniciociclo: function () {
      bate = new Promise(function (resolve, reject) {
        pic32.on('SWLOW', function (payload) {
          resolve();
        });
      });
      // return timeoutPromise(5000, bate);
      return promiseTimeout(5000, bate)

      // Com ambas, dá um Warning, mas funciona bem, aparentemente.
      // https://github.com/jakesgordon/javascript-state-machine/issues/169

    },
    // Só salta para o estado 'umquarto' se receber o evento 'DOWN'
    onLeaveUmquarto: function () {
      var sobe = new Promise(function (resolve, reject) {
        pic32.on('SWTOP', function (payload) {
          resolve();
        });
      });
      var empurra = new Promise(function (resolve, reject) {
        pic32.on('SWRIGHT', function (payload) {
          resolve();
        });
      });
      return Promise.all([sobe, empurra]);
    },

  }
});

pic32.on('START', function (payload) {
  switch (bb.state) {
    case 'arranque':
      if (SWALEFT && SWARIGHT && SWFLEFT && SWFRIGHT) {
        //  falta SWTOP && SWLEFT
        bb.prepara();
      } else {
        console.log('Testing SWALEFT && SWARIGHT && SWFLEFT && SWFRIGHT failed')
      }
      break;
    case 'iniciociclo':
      bb.bate();
    default:
      console.log('Signal START DISCARDED')
  }
});

// para gerar o grafo com o dot:
bb.visualize = function () {
  return visualize(bb)
}

// Ligar LED (máquina ligada, operação parada)
port.write(outputs['LOFF'] + ' on');
port.write(outputs['MOTOR'] + ' on');

// forçar leituras:
var forcereadsignals = {
  'START': 'cafeina/din/1',
  'STOP': 'cafeina/din/2',
  'AUT': 'cafeina/din/3',
  'SWTOP': 'cafeina/din/4',
  'SWLOW': 'cafeina/din/5',
  'SWLEFT': 'cafeina/din/6',
  'SWRIGHT': 'cafeina/din/7',
  'MOTPROT': 'cafeina/din/8',
  'SWFLEFT': 'cafeina/din/9',
  'SWFRIGHT': 'cafeina/din/10',
  'EMERG': 'cafeina/din/11',
  'SWALEFT': 'cafeina/din/12',
  'SWARIGHT': 'cafeina/din/13'
};


var init = function() {
  setTimeout(function () { port.write(forcereadsignals['SWALEFT']); }, 50);
  setTimeout(function () { port.write(forcereadsignals['SWARIGHT']); }, 100);
  setTimeout(function () { port.write(forcereadsignals['SWFLEFT']); }, 150);
  setTimeout(function () { port.write(forcereadsignals['SWFRIGHT']); }, 200);
}

setTimeout(init, 2000);

/* 
node -i -e "$(< bb.js)"
node
.load ./bb.js */

// https://wiki.geomaster.pt/index.php?title=PIC32

/*
 * Controlo

 * Ligar saída digital (24v): cafeina/dout/3 on
 * Desligar saída digital (24v): cafeina/dout/3 off

 */

/*
  * Mensagens (automáticas) do RoqDry:

  * bicafe/din/3 on
  * Forçar leitura: cafeina/din/3 (e ele há-de responder com bicafe/din/3 on|off)

  */

